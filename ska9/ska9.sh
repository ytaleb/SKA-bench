#sudo ./ska9.sh ../ssd/ 1 1G 1M 1000 1024 1024
if [ $# -ne 6 ]; then
    echo "Usage: dir numFiles largeFileSize smallFileSize processes maxOutstandingIOs
    lowTierRate(Read)"
    exit
fi

sync
Directory=$1            \
NumFiles=$2             \
LargeFileSize=$3          \
SmallFileSize=$4          \
Processes=$5            \
OutStanding=$6          \
fio ska9.fio

