
# sudo ./ska1.sh ./ 1 1G 1000 

sync

if [ $# -ne 4 ]; then
	echo "Usage: filepath nbFiles maxFilesize(min=4MB) outstadingIOs"
	exit
fi

DIRECTORY=$1 NUMFILES=$2 MAXFILESIZE=$3 OUTSTANDING=$4 fio ska1.fio

