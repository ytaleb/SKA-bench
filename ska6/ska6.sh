# Emulates writing memory state to persistent storage
# sudo ./ska6.sh ../ssd/testFile 1G > ska6.log


if [ $# -ne 2 ];
then
	echo "usage: fileName fileSize"
	exit
fi
sync
FILENAME=$1 FILESIZE=$2 fio ska6.fio
