if [ $# -ne 2 ];
then
        echo "usage: targetDir logdir"
        exit
fi

dir=$1
logs=$2
outstanding=1000
nbfiles=1
maxfilesize=1G
minfilesize=1M
lowTierRateRead=1024
lowTierRateWrite=1024

mkdir ./logs
cd ska1
./ska1.sh $dir $nbfiles $maxfilesize $outstanding > $logs/ska1.log
cd ../ska2
./ska2.sh $dir $nbfiles $nbfiles $nbfiles $outstanding > $logs/ska2.log
cd ../ska4
./ska4.sh $dir $nbfiles $maxfilesize $minfilesize $outstanding $lowTierRateWrite $lowTierRateRead > $logs/ska4.log
cd ../ska5
./ska5.sh $dir $nbfiles $minfilesize $maxfilesize > $logs/ska5.log
cd ../ska6
./ska6.sh $dir/testFile $maxfilesize > $logs/ska6.log
