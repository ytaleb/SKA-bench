## A suit of FIO benchmarks for the Square Kilometre Array (SKA) project.

#### Requirements:

Fio installed (clone git with recursive, then install fio)

apt-get install libaio-dev

A path needs to be passed as paremeters for every test-case. 
To use multiple devices, consider mounting different folders on different devices, e.g. ./ssd/, ./ram/ etc. 


### Assumptions 
1. We assume 4M blocksize for large files and 4K for small files. This holds both for sequential and random access.
2. We assume 1 job that spawns multiple I/O units (iodepth)
3. Asynchronous operations using libaio engines 
4. Values are set for the operational range of SSDs
    -> iodepth[100,1000], HDD[16,64], [246,4096]
5. Precision is not needed, thus we reduce gettimeofday calls
6. The process terminates if any error occurs
7. We assume cache management within the application (e.g databases), thus we use direct I/O
8. No assumption as to the layout of a block. If we write 1M, we must read back 1M.
    -> Avoid encryption issues, permutations, endianess blah blah blah.
    -> It would be possible to write seq 1M and for rand read to use smaller block size,
    but it makes things more complicated.
9. For every write/read, a different filesize is used (within a bounded range)



### Docs 
https://github.com/axboe/fio/blob/master/HOWTO

http://www.n0derunner.com/2014/06/multiple-devicesjobs-in-fio/

https://thisdataguy.com/tag/readahead/
