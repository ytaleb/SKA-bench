# sudo ./ska5.sh ../ssd/ 1 4M 256M > ska5.log

if [ $# -ne 4 ]; then
	echo "Usage: dir numFiles sizeRangeMin sizeRangeMax"
	exit
fi

sync
DIRECTORY=$1 NUMFILES=$2 SIZERANGEMIN=$3 SIZERANGEMAX=$4 fio phase1.fio

sync
DIRECTORY=$1 NUMFILES=$2 SIZERANGEMIN=$3 SIZERANGEMAX=$4 fio phase2.fio
