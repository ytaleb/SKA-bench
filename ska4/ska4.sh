# Example run: ./ska4.sh ~/shared/fs50gb.1/xxx/ 30 100M 10M 1 200m 10m


# sudo ./ska4.sh ../ssd/ 1 1G 1M 1000 1024 1024
if [ $# -ne 7 ]; then
	echo "Usage: dir numFiles largeFileSize smallFileSize maxOutstandingIOs lowTierRate(Write) lowTierRate(Read)"
	exit
fi

sync
Directory=$1            \
NumFiles=$2             \
LargeFileSize=$3          \
SmallFileSize=$4          \
OutStanding=$5          \
LowerTierWriteRate=$6   \
LowerTierReadRate=$7    \
fio ska4.fio

