Could not find a way within fio for prefetch tuning -readahead parameter.
Probably this should be tuned on the underlying storage

# For block devices
- Increase readahead
sudo blockdev --setra 65536 /dev/sdxx

- Disable scheduler
echo 'deadline' > /sys/block/sdxx/queue/scheduler


# For filesystems
- Maximum number of inodes that will pre-read into buffer cache
mount -o inode_readahead=n BLOCK DST

