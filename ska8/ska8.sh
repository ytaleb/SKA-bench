# loading data from lower to upper tier 
# sudo ./ska8.sh Directory SmallFileSize NumFiles OutStanding  LowerTierReadRate

sync

if [ $# -ne 5 ]; then
	echo "Usage: Directory fileSize NumFiles outstandingIOs  LowerTierReadRate"
	exit
fi

Directory=$1 SmallFileSize=$2 NumFiles=$3 OutStanding=$4  LowerTierReadRate=$5 fio ska8.fio
